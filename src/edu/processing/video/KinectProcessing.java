/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.processing.video;

import blobscanner.Detector;
import edu.gui.utils.JPicture;
import edu.gui.principal.JPrincipal;
import edu.processing.KinectTracker;
import org.openkinect.processing.Kinect;
import org.openkinect.processing.Kinect2;
import processing.core.PApplet;

/**
 *
 * @author zinho
 */
// Daniel Shiffman
// Tracking the average location beyond a given depth threshold
// Thanks to Dan O'Sullivan
// https://github.com/shiffman/OpenKinect-for-Processing
// http://shiffman.net/p5/kinect/
public class KinectProcessing extends PApplet implements edu.processing.KinectProcessing {

    private static Kinect kinect;
    private Detector bd;

    private int contL = 0;

    @Override
    public Kinect getKinect() {
        return kinect;
    }

 

    @Override
    public void settings() {
        size(640, 520);
    }

    @Override
    public void setup() {
        kinect = new Kinect(this);
        kinect.initDepth();
        kinect.initVideo();
        kinect.enableColorDepth(true);
    }

    @Override
    public void draw() {
        background(0);
        image(kinect.getVideoImage(), 0, 0);
        image(kinect.getDepthImage(), 640, 0);
        fill(255);
    }

    @Override
    public void setTilt(float tilt) {
        getKinect().setTilt(tilt);
    }

    @Override
    public KinectTracker getKinectTracker() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setJPicture(JPicture jPicture) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setHandTracker(boolean handTracker) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setJPrincipal(JPrincipal jPrincipal) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  

}
