/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.processing;

import processing.core.PImage;
import processing.core.PVector;

/**
 *
 * @author zinho
 */
// Daniel Shiffman
// Tracking the average location beyond a given depth threshold
// Thanks to Dan O'Sullivan
// https://github.com/shiffman/OpenKinect-for-Processing
// http://shiffman.net/p5/kinect/
public interface KinectTracker  {
    
    public void track();

    public PVector getLerpedPos();

    public PVector getPos();

    public PImage getDisplay();

    public void display();
    
    public void followHand();

    public int getThreshold();

    public void setThreshold(int t);
    
    public void setHandTracker(boolean handTracker);
    
}
