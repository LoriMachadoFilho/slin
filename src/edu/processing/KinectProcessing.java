/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.processing;

import edu.gui.utils.JPicture;
import edu.gui.principal.JPrincipal;
import org.openkinect.processing.Kinect;


/**
 *
 * @author zinho
 */

public interface KinectProcessing {

    public Kinect getKinect();

    public KinectTracker getKinectTracker();
    
    public void settings();
    
    public void setup();

    public void draw();

    public void keyPressed();
    
    public void setTilt(float tilt);   
    
    public void setJPicture(JPicture jPicture);   
    
    public void setJPrincipal(JPrincipal jPrincipal);   
    
    public void setHandTracker(boolean handTracker);
    
    public void stop();
 
}
