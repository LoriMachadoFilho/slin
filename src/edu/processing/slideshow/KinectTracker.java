/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.processing.slideshow;

import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;

/**
 *
 * @author zinho
 */
// Daniel Shiffman
// Tracking the average location beyond a given depth threshold
// Thanks to Dan O'Sullivan
// https://github.com/shiffman/OpenKinect-for-Processing
// http://shiffman.net/p5/kinect/
public class KinectTracker implements edu.processing.KinectTracker {
    protected int pixelCount;
    private boolean handTracker;
    private boolean zoomEnabled;
    private int tilt;
    private int zoom;
    private int zFixed;
    private boolean zoomIn;
    private boolean zoomOut;
    
    // Depth threshold
    private int threshold = 700;

    // Raw location
    private PVector loc;

    // Interpolated location
    private PVector lerpedLoc;

    // Depth data
    private int[] depth;

    // What we'll show the user
    private PImage display;

    private KinectProcessing kinectProcessing;

    public KinectTracker(KinectProcessing kinectProcessing) {
        this.kinectProcessing = kinectProcessing;
        // This is an awkard use of a global variable here
        // But doing it this way for simplicity
        kinectProcessing.getKinect().initDepth();
        
        kinectProcessing.getKinect().enableMirror(true);
        // Make a blank image
        display = kinectProcessing.createImage(kinectProcessing.getKinect().width, kinectProcessing.getKinect().height, kinectProcessing.RGB);
        // Set up the vectors
        loc = new PVector(0, 0);
        lerpedLoc = new PVector(0, 0);
    }

    public void track() {

        // Get the raw depth as array of integers
        depth = kinectProcessing.getKinect().getRawDepth();

        // Being overly cautious here
        if (depth == null) {
            return;
        }

        float sumX = 0;
        float sumY = 0;
        int midleZ = 0;
        pixelCount = 0;
        for (int x = 0; x < kinectProcessing.getKinect().width; x++) {
            for (int y = 0; y < kinectProcessing.getKinect().height; y++) {

                int offset = x + y * kinectProcessing.getKinect().width;
                // Grabbing the raw depth
                int rawDepth = depth[offset];

                // Testing against threshold
                if (rawDepth < threshold) {
                    sumX += x;
                    sumY += y;
                    pixelCount++;
                }
            }
        }

        // As long as we found something
        if (pixelCount != 0) {
            loc = new PVector(sumX / pixelCount, sumY / pixelCount);
            Float offset = (sumX / pixelCount) + (sumY / pixelCount) * kinectProcessing.getKinect().width;
            int offsetAux = offset.intValue();
            midleZ = depth[offsetAux];
        }

        // Interpolating the location, doing it arbitrarily for now
        lerpedLoc.x = PApplet.lerp(lerpedLoc.x, loc.x, 0.3f);
        lerpedLoc.y = PApplet.lerp(lerpedLoc.y, loc.y, 0.3f);

        //zoom
        if(zoomEnabled){
            if(zoom == 0){
                zFixed = midleZ;
                zoom = 1;
            }
            System.out.println("zfixed = " + this.zFixed);
            System.out.println("midlez = " + midleZ);
            if(zFixed < (midleZ - 100)){
                zFixed = midleZ;
                zoomOut = true;
            } else if(zFixed > (midleZ + 100)){
                zFixed = midleZ;
                zoomIn = true;          
            } else {
                zoomIn = zoomOut = false;
            }
            
            
        } else {
            zoom = 0;
        }
        
//        kinectProcessing.translate(-loc.x * 2.2f, -loc.y * 2f);
        if (handTracker) {
            followHand();
        }
    }

    public PVector getLerpedPos() {
        return lerpedLoc;
    }

    public PVector getPos() {
        return loc;
    }

    public PImage getDisplay() {
        return this.display;
    }

    public void display() {
        PImage img = kinectProcessing.getKinect().getDepthImage();
//        float scal = 3.3f;
//        kinectProcessing.scale(scal);
        // Being overly cautious here
        if (depth == null || img == null) {
            return;
        }
        
        // Going to rewrite the depth image to show which pixels are in threshold
        // A lot of this is redundant, but this is just for demonstration purposes
        display.loadPixels();
        for (int x = 0; x < kinectProcessing.getKinect().width; x++) {
            for (int y = 0; y < kinectProcessing.getKinect().height; y++) {

                int offset = x + y * kinectProcessing.getKinect().width;
                // Raw depth
                int rawDepth = depth[offset];
                int pix = x + y * display.width;
                if (rawDepth < threshold) {
                    // A red color instead
                    display.pixels[pix] = kinectProcessing.color(255, 255, 255);
                } else {
                    display.pixels[pix]= kinectProcessing.color(0, 0, 0); // = img.pixels[offset]
                }
            }
        }
        display.updatePixels();

        // Draw the image
        kinectProcessing.image(display, 0, 0);
    }
    
    public void followHand(){
        if (pixelCount > 5000) {
            if (loc.y < 180) {
                kinectProcessing.getKinect().setTilt(++tilt);
            } else if (loc.y > 300) {
                kinectProcessing.getKinect().setTilt(--tilt);
            }
        }
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int t) {
        threshold = t;
    }
    
    public void setHandTracker(boolean handTracker){
        this.handTracker = handTracker;
    }
    
    public void setZoomEnabled(boolean zoomEnabled){
        this.zoomEnabled = zoomEnabled;        
    }
    
    public boolean getZoomIn(){
        return this.zoomIn;
    }
    
    public boolean getZoomOut(){
        return this.zoomOut;
    }
    
    public int getTilt(){
        return this.tilt;
    }
}
