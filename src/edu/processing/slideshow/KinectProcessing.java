/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.processing.slideshow;

import blobscanner.Detector;
import edu.gui.utils.JYesNo;
import edu.gui.utils.JPicture;
import edu.gui.principal.JPrincipal;
import edu.gui.utils.JMsg;
import edu.utils.Constants;
import edu.utils.InfoKinectInUse;
import java.awt.Frame;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;
import org.openkinect.processing.Kinect;
import processing.core.PApplet;
import static processing.core.PConstants.CODED;
import static processing.core.PConstants.DOWN;
import static processing.core.PConstants.UP;
import processing.core.PVector;
import ssu.log.LogReport;

/**
 *
 * @author zinho
 */
// Daniel Shiffman
// Tracking the average location beyond a given depth threshold
// Thanks to Dan O'Sullivan
// https://github.com/shiffman/OpenKinect-for-Processing
// http://shiffman.net/p5/kinect/
public class KinectProcessing extends PApplet implements edu.processing.KinectProcessing {

    private static KinectTracker tracker;
    private static Kinect kinect;
    private float x = 0;
    private float xAnt = 0;
    private float y = 0;
    private static JPicture jPicture;
    private int movCount;
    private int movCountAux;
    private int match = 0;
    private float xMiddleGreaterBlob;
    private float yMiddleGreaterBlob;
    private Detector bd;
    private boolean yes = false;
    private Thread yesNoThread;
    private int yesNoDelay = Constants.YES_DELAY;
    private int loadInfoDelay = Constants.LOAD_INFO_KINECT_IN_USE_DELAY;
    private boolean isAHand;
    private PVector v1;
    private int pixelsGreaterBlob;
    private float yGreater = 0f;
    private float ySmaller = 10000f;
    private float xGreater = 0f;
    private float xSmaller = 10000f;
    private static JPrincipal jPrincipal;
    private int blobsNumber;
    private final int PIXEL_COUNT_THRESHOLD_MIN_LOCAL = Constants.PIXEL_COUNT_THRESHOLD_MIN;
    private final int PIXEL_COUNT_THRESHOLD_MAX_LOCAL = Constants.PIXEL_COUNT_THRESHOLD_MAX;
    private int blackFrame = 30;
    private JMsg jMsg;

    @Override
    public Kinect getKinect() {
        return kinect;
    }

    @Override
    public edu.processing.KinectTracker getKinectTracker() {
        return tracker;
    }

    @Override
    public void settings() {
        size(640, 520);
    }

    @Override
    public void setup() {
        kinect = new Kinect(this);
        kinect.initDepth();
        kinect.enableColorDepth(true);
        tracker = new KinectTracker(this);
        bd = new Detector(this, 640);
        initYesNoThread();
    }

    private int debug = 30;
    private int slow = 0;
    private int fast = 0;

    @Override
    public void draw() {
        background(255);
        x = xMiddleGreaterBlob;
        movCount++;

        findHandThreshold();

        if(Constants.DEBUG){
            debug--;
            if (debug < 0) {
                debug = 30;
                this.frame.setVisible(true);
            }
        }
        if (xAnt != 0) {
            if(tracker.pixelCount > Constants.PIXEL_COUNT_TRACK_MIN && yes){
                if ((xAnt - x) > Constants.HAND_MOVE_MIN && (xAnt - x) < Constants.HAND_MOVE_MAX ) {
                    if (jPicture != null && (Math.abs(movCount - movCountAux) > Constants.PICTURE_REFRESH_TIME)) {
                        try {
                            jPicture.moveLeft();
                            movCountAux = movCount;
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(null, "Erro ao mover para a esquerda. \n" + ex.getMessage());
                        }
                    } 
                    slow = fast = 0;
                } else if ((xAnt - x) > 20 && (xAnt - x) < Constants.HAND_MOVE_MIN) {
                    fast = 0;
                    slow++;
                    if(slow > 2){
                        slow = 0;
                        showTooSlowMsg();
                    }
                } else if ((xAnt - x - 20) > Constants.HAND_MOVE_MAX) {
                    slow = 0;
                    fast++;
                    if(fast > 1){
                        fast = 0;
                        showTooFastMsg();
                    }
                }
            }
        }

        if (movCount >= Integer.MAX_VALUE) { //Para n�o estourar a mem�ria
            movCount = movCountAux = 0;
        }

        // Run the tracking analysis
        tracker.track();
        // Show the image
        tracker.display();
        trackZoom();

        // Desenha um circulo no meio dos pixels encontrados pelo Tracker
        v1 = tracker.getPos();
        fill(50, 100, 250, 200);
        noStroke();
        ellipse(v1.x, v1.y, 20, 20);

        loadInfoDelay--;
        if (loadInfoDelay < 0) {
            loadInfoDelay = Constants.LOAD_INFO_KINECT_IN_USE_DELAY;
            loadInfoKinectInUse();
    }
        yesNoDelay--;
        if (yesNoDelay < 0) {
            if ((xMiddleGreaterBlob - Constants.HAND_THRESHOLD) < v1.x && (xMiddleGreaterBlob + Constants.HAND_THRESHOLD) > v1.x
                    && (yMiddleGreaterBlob - Constants.HAND_THRESHOLD) < v1.y && (yMiddleGreaterBlob + Constants.HAND_THRESHOLD) > v1.y
                    && isAHand) {
                yesNoDelay = Constants.YES_DELAY;
                yes = true;
            } else {
                yesNoDelay = Constants.YES_DELAY;
                yes = false;
            }
        }

        xAnt = x;
    }

    private void trackZoom() {
        bd.findBlobs(tracker.getDisplay().pixels, 640, 480);

        bd.loadBlobsFeatures();
        bd.findCentroids();

        int totalPossibleMatches = 0;
        int totalMatches = 0;
        int maiorBlob = 0;
        int maiorBlob2 = 0;
        int blob = 0;
        int n = 0;
        blobsNumber = bd.getBlobsNumber();
        while (blobsNumber != 0 && blobsNumber > n) {
            if (bd.getEdgePoints(n).length > blob) {
                blob = bd.getEdgePoints(n).length;
                maiorBlob2 = maiorBlob;
                maiorBlob = n;
            }
            n++;
        }
        if (Constants.ONLY_ONE_BLOB) {
            if (blobsNumber > 1) {
                int tamanhoBlob2 = bd.getEdgePoints(maiorBlob2).length;
                int tamanhoBlob = bd.getEdgePoints(maiorBlob).length;
                if (tamanhoBlob != 0 && tamanhoBlob2 > 10 && tamanhoBlob2 < 500) {
                    if (Constants.PIXEL_COUNT_THRESHOLD_MIN >= 1500) {
                        Constants.PIXEL_COUNT_THRESHOLD_MIN -= 100;
                    }
                    if (Constants.PIXEL_COUNT_THRESHOLD_MAX >= 1500) {
                        Constants.PIXEL_COUNT_THRESHOLD_MAX -= 100;
                    }
                    if (tamanhoBlob < 1000) {
                        Constants.PIXEL_COUNT_THRESHOLD_MIN += 200;
                        Constants.PIXEL_COUNT_THRESHOLD_MAX += 200;
                    }
                    jPrincipal.setPixelCountThresholdMinMax(Constants.PIXEL_COUNT_THRESHOLD_MIN, Constants.PIXEL_COUNT_THRESHOLD_MAX);
                }
            }
            if (blobsNumber == 0) {
                blackFrame--;
                if (blackFrame < 0) {
                    blackFrame = 30;
                    Constants.PIXEL_COUNT_THRESHOLD_MIN = PIXEL_COUNT_THRESHOLD_MIN_LOCAL;
                    Constants.PIXEL_COUNT_THRESHOLD_MAX = PIXEL_COUNT_THRESHOLD_MAX_LOCAL;
                    jPrincipal.setPixelCountThresholdMinMax(Constants.PIXEL_COUNT_THRESHOLD_MIN, Constants.PIXEL_COUNT_THRESHOLD_MAX);
                }
            }
        }
//        if (blobsNumber > 20) {
//            showTooManyBlobsMsg();
//        }
        float sumx = 0;
        float sumy = 0;
        if (blobsNumber != 0) {
            pixelsGreaterBlob = 0;
            PVector[] edge = bd.getEdgePoints(maiorBlob);

            totalPossibleMatches = edge.length;
            totalMatches = 0;
            yGreater = 0f;
            ySmaller = 10000f;
            xGreater = 0f;
            xSmaller = 10000f;
            for (int i = 0; i < (edge.length); i++) {
                if (edge[i].y < ySmaller) {
                    ySmaller = edge[i].y;
                }
                if (edge[i].y > yGreater) {
                    yGreater = edge[i].y;
                }
                if (edge[i].x < xSmaller) {
                    xSmaller = edge[i].x;
                }
                if (edge[i].x > xGreater) {
                    xGreater = edge[i].x;
                }
                sumx += edge[i].x;
                sumy += edge[i].y;
                pixelsGreaterBlob++;
                //Para n�o estourar o array
                if (i >= 2200) {
                    continue;
                }
            }
            isAHand = Math.abs(yGreater - ySmaller) < Constants.PIXEL_HEIGHT;
            isAHand = isAHand ? Math.abs(xGreater - xSmaller) < Constants.PIXEL_WIDTH : false;
            isAHand = !isAHand ? false : tracker.pixelCount > 1000;
            xMiddleGreaterBlob = sumx / pixelsGreaterBlob;
            yMiddleGreaterBlob = sumy / pixelsGreaterBlob;

            //Cria o circulo no centro do maior Blob
            fill(255, 30, 30, 100);
            noStroke();
            ellipse(xMiddleGreaterBlob, yMiddleGreaterBlob, 20, 20);

            if (totalPossibleMatches != 0) {
                DecimalFormat dc = new DecimalFormat("0.00");
                double percentual = (totalMatches * 100d) / totalPossibleMatches;
                text(dc.format(percentual), tracker.getPos().x, tracker.getPos().y);
                fill(0, 0, 0, 0);
                if (percentual > 90) {
                    match++;
                } else {
                    match = 0;
                    text("ZOOM INACTIVATED", tracker.getPos().x, tracker.getPos().y);
                }
                if (match > 10) {
                    System.out.println("zoom activaded");
                    text("ZOOM ACTIVATED", tracker.getPos().x, tracker.getPos().y);
                }
                tracker.setZoomEnabled(match > 10);
                if (match > 11) {
                    System.out.println("match " + match);
                    if (tracker.getZoomIn()) {
                        System.out.println("zoom in true");
                        jPicture.zoom(true);
                    }
                    if (tracker.getZoomOut()) {
                        System.out.println("zoom out true");
                        jPicture.zoom(false);
                    }
                }
            }

        }

    }
    
    private void showTooManyBlobsMsg() {
        showMsg(edu.utils.HelpMsg.TOO_MANY_BLOBS);
    }

    private void showTooFastMsg() {
        showMsg(edu.utils.HelpMsg.TOO_FAST);
    }

    private void showTooSlowMsg() {
        showMsg(edu.utils.HelpMsg.TOO_SLOW);
    }

    private void showMsg(String msg){
        if (jMsg != null || !Constants.HELP_MSG) {
            return;
        }
        jMsg = new JMsg(msg);
        new Thread(new Runnable() {
            @Override
            public void run() {
                jMsg.setVisible(true);
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {

                } finally {

                    jMsg.dispose();
                    jMsg = null;
                }
            }
        }).start();
    }

    private InfoKinectInUse infoKinectInUse;

    private void loadInfoKinectInUse() {
        infoKinectInUse = new InfoKinectInUse();
        infoKinectInUse.PICTURE_REFRESH_TIME = Constants.PICTURE_REFRESH_TIME - Math.abs(movCount - movCountAux);
        if (infoKinectInUse.PICTURE_REFRESH_TIME < 0) {
            infoKinectInUse.PICTURE_REFRESH_TIME = 0;
        }
        infoKinectInUse.HAND_THRESHOLD_X = Float.valueOf(Math.abs(xMiddleGreaterBlob - v1.x)).intValue();
        infoKinectInUse.HAND_THRESHOLD_Y = Float.valueOf(Math.abs(yMiddleGreaterBlob - v1.y)).intValue();
        infoKinectInUse.HAND_MOVE = Float.valueOf(Math.abs(xAnt - x)).intValue();
        infoKinectInUse.PIXEL_COUNT_TRACK = tracker.pixelCount;
        infoKinectInUse.PIXEL_COUNT_THRESHOLD = pixelsGreaterBlob;
        infoKinectInUse.YES_DELAY = yesNoDelay;
        if (infoKinectInUse.YES_DELAY < 0) {
            infoKinectInUse.YES_DELAY = 0;
        }
        infoKinectInUse.PIXEL_HEIGHT = Float.valueOf(Math.abs(yGreater - ySmaller)).intValue();
        infoKinectInUse.PIXEL_WIDTH = Float.valueOf(Math.abs(xGreater - xSmaller)).intValue();
        infoKinectInUse.BLOBS_NUMBER = blobsNumber;
        infoKinectInUse.FRAME_RATE = Float.valueOf(frameRate).intValue();
        infoKinectInUse.THRESHOLD = tracker.getThreshold();
        infoKinectInUse.TILT = tracker.getTilt();

        if (jPrincipal != null) {
            jPrincipal.loadInfoKinectInUse(infoKinectInUse);
        }

    }

    private void findHandThreshold() {
            if (tracker.pixelCount < Constants.PIXEL_COUNT_THRESHOLD_MIN && tracker.getThreshold() < Constants.THRESHOLD_MAX) {
                tracker.setThreshold(tracker.getThreshold() + 3);
            } else if (tracker.pixelCount > Constants.PIXEL_COUNT_THRESHOLD_MAX) {
                tracker.setThreshold(tracker.getThreshold() - 3);
            }
        
    }

//    private int noCount =0;
    private JYesNo yesNo = null;

    private void initYesNoThread() {
        if (yesNo == null) {
            yesNo = new JYesNo(0, 0);
        }
        yesNoThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(500);
                        if (yes) {
//                            noCount = 0;
                            yesNo.displayYes();
                        } else {
//                            noCount++;
//                            if(noCount > 6){
//                                noCount = 0;
                            yesNo.displayNo();
//                            }
                        }
//                        System.out.println(noCount);
                    } catch (InterruptedException ie) {
                        break;
                    } catch (Exception e) {
                        LogReport.insertLog("Erro na Thread YesNo.", e, false);
                    }
                }
            }
        });
        yesNoThread.start();
    }

    @Override
    public void setJPrincipal(JPrincipal jPrincipal) {
        this.jPrincipal = jPrincipal;
    }

// Adjust the threshold with key presses
    @Override
    public void keyPressed() {
        int t = tracker.getThreshold();
        if (key == CODED) {
            if (keyCode == UP) {
                t += 5;
                tracker.setThreshold(t);
            } else if (keyCode == DOWN) {
                t -= 5;
                tracker.setThreshold(t);
            }
        }
    }

    @Override
    public void setTilt(float tilt) {
        getKinect().setTilt(tilt);
    }

    @Override
    public void setJPicture(JPicture jPicture) {
        this.jPicture = jPicture;
    }

    @Override
    public void setHandTracker(boolean handTracker) {
        tracker.setHandTracker(handTracker);
    }

    @Override
    public Frame getFrame() {
        return this.frame;
    }

    @Override
    public void stop() {
        if (kinect == null || tracker == null) {
            return;
        }
        yesNo.dispose();
        kinect.enableColorDepth(false);
        kinect.stopDepth();
        kinect.stopVideo();
        frame.dispose();

        tracker = null;
        kinect = null;
        yesNo = null;
    }

}
