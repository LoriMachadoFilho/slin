/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.gui.principal;

import edu.gui.utils.JPicture;
import edu.utils.Config;
import edu.utils.Constants;
import edu.utils.InfoKinectInUse;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import javax.swing.ListSelectionModel;
import processing.core.PApplet;
import ssu.log.LogReport;
import ssu.table.MyTableModel;
import ssu.table.MyTableSorter;

/**
 *
 * @author Zinho
 */
public class JPrincipal extends javax.swing.JFrame {

    public static edu.processing.KinectProcessing kinectProcessing = null;
    private ArrayList<File> list = new ArrayList();
    /**
     * Creates new form JPrincipal
     */
    public JPrincipal() {
        initComponents();
        js_Segundos.setValue(Constants.AUTOMATIC_SLIDE_TIME);
        jl_SO.setText(edu.utils.OperationalSystem.OS_NAME_ARCH);
        edu.utils.Kinect.keepCheckingKinect(this, true);
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (e.getKeyChar() == KeyEvent.VK_F5) {
                    slideShow();
                }
            }
        });
        loadConfig();
        loadPictures();
        if(Constants.AUTOMATIC_INIT && jt_File.getRowCount() > 0){
            slideShow();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                    }
                    p.setVisible(true);
                }
            }).start();
        }
    }

    private void tiltUpDown(float tilt) {
     
        try {
            if(kinectProcessing == null){
                LogReport.insertLog("O Kinect n�o foi ativado. Por favor, verifique.", null, true);
                return;
            }            
            kinectProcessing.setTilt(tilt);
        } catch (Exception e) {
            LogReport.insertLog("Erro ao executar o motor do Kinect.", e, true);
        }
    }

    private void turnOnOff() {
        if (kinectProcessing == null) {
            PApplet.main("edu.processing.slideshow.KinectProcessing");
            kinectProcessing = new edu.processing.slideshow.KinectProcessing();
            kinectProcessing.setTilt(0);
            kinectProcessing.setJPrincipal(this);
        } else {
            kinectProcessing.getKinect().stopDepth();
            kinectProcessing.getKinect().stopVideo();
            kinectProcessing = null;
        }
    }

    public void setTextButtonOnOff(String text){
        jb_onOff.setText(text);
        if(text.equals("Sim")){
            jb_onOff.setForeground(edu.utils.Colors.GREEN);
        } else {
            jb_onOff.setForeground(edu.utils.Colors.RED);            
        }
    }
    
    private JPicture p = null;
    private void slideShow(){
        if(list.isEmpty()){
            LogReport.insertLog("Nenhuma Imagem foi selecionada.", null, true);
            return;
        }
          if (kinectProcessing == null || kinectProcessing.getKinect() == null) {
            PApplet.main("edu.processing.slideshow.KinectProcessing");
            kinectProcessing = new edu.processing.slideshow.KinectProcessing();
            kinectProcessing.setTilt(0);
            
            try {
                p = new JPicture();
                for (File file : list) {
                    p.putImage(file.getAbsolutePath());
                }
                p.setVisible(true);
                p.initSlideShow();
                p.setCircular(this.jcb_Infinito.isSelected());
                p.setTime((Integer)js_Segundos.getValue());
                p.setJPrincipal(this);
                kinectProcessing.setJPicture(p);
                kinectProcessing.setJPrincipal(this);
                kinectProcessing.setHandTracker(Constants.HAND_TRACKER);
                
            } catch (Exception ex) {
                LogReport.insertLog("Erro ao abrir Slide Show.", ex, true);
            }
        } else {
            stopKinect();
        }
    }
   
    public void stopKinect() {        
        kinectProcessing.stop();        
        kinectProcessing = null;
    }
    
     private void initFileTable() {
        // Set Default Table Model
        MyTableModel tm = new MyTableModel();

        // Set Columns
        tm.addColumn("Nome da Imagem");
        tm.addColumn("Object");

        MyTableSorter ts = new MyTableSorter(tm);
        jt_File.setModel(ts);
        ts.setTableHeader(jt_File.getTableHeader());

        // Set Columns Width        
        jt_File.getColumn("Nome da Imagem").setPreferredWidth(450);

        // Hide Object
        jt_File.removeColumn(jt_File.getColumn("Object"));
    }

    private void refreshFileTable() {
        initFileTable();

        try {
            jt_File.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

            MyTableSorter ts = (MyTableSorter) jt_File.getModel();
            ts.reset();
            
            for (int i = 0; i < list.size(); i++) {
                File file = list.get(i);
                Object[] row = new Object[2];
                row[0] = file.getName();               
                row[1] = file; // The object by itself

                ts.addRow(row);
            }
        } catch (Exception e) {
            LogReport.insertLog("Erro ao atualizar a tabela de Arquivos.", e, false);
            throw e;
        }
    }
    
    private void deleteFileFromList(){
        try {
            if(jt_File.getSelectedRow() == -1){
                throw new Exception("Nenhum arquivo selecionado.");
            }
            File file = (File) jt_File.getModel().getValueAt(jt_File.getSelectedRow(), jt_File.getModel().getColumnCount() -1);
            list.remove(file);
            refreshFileTable();
        } catch (Exception e) {
            LogReport.insertLog("Erro ao deletar um Arquivo.", e, true);
        }
    }
    
    private void keyTypedAtList(java.awt.event.KeyEvent evt){
        if(evt.getKeyChar() == KeyEvent.VK_DELETE){
            deleteFileFromList();
        }
    }
    
    private void addImages(){
        jfc_ListaArquivo.setMultiSelectionEnabled(true);
        int returnVal = jfc_ListaArquivo.showOpenDialog(this);
        if (returnVal == jfc_ListaArquivo.APPROVE_OPTION) {
            File[] files = jfc_ListaArquivo.getSelectedFiles();
            for (File file : files) {
                if(!list.contains(file)){
                    list.add(file);
                }
            }
            refreshFileTable();
        }
    }
    
    private void loadConfig(){
        edu.utils.Constants.loadConstants();        
        jtf_PICTURE_REFRESH_TIME.setText(Constants.PICTURE_REFRESH_TIME + "");
        jtf_HAND_THRESHOLD.setText(Constants.HAND_THRESHOLD + "");
        jtf_HAND_MOVE_MIN.setText(Constants.HAND_MOVE_MIN + "");
        jtf_HAND_MOVE_MAX.setText(Constants.HAND_MOVE_MAX + "");
        jtf_PIXEL_COUNT_TRACK_MIN.setText(Constants.PIXEL_COUNT_TRACK_MIN + "");
        jtf_PIXEL_COUNT_THRESHOLD_MIN.setText(Constants.PIXEL_COUNT_THRESHOLD_MIN + "");
        jtf_PIXEL_COUNT_THRESHOLD_MAX.setText(Constants.PIXEL_COUNT_THRESHOLD_MAX + "");
        jtf_YES_DELAY.setText(Constants.YES_DELAY + "");
        jtf_PIXEL_HEIGHT.setText(Constants.PIXEL_HEIGHT + "");
        jtf_PIXEL_WIDTH.setText(Constants.PIXEL_WIDTH + "");
        jtf_LOAD_INFO_KINECT_IN_USE_DELAY.setText(Constants.LOAD_INFO_KINECT_IN_USE_DELAY + "");
        jcb_OnlyOneBlob.setSelected(Constants.ONLY_ONE_BLOB);
        jcb_HelpMsg.setSelected(Constants.HELP_MSG);
        jcb_Debug.setSelected(Constants.DEBUG);
        jcb_HankTracker.setSelected(Constants.HAND_TRACKER);
        jcb_AutomaticInit.setSelected(Constants.AUTOMATIC_INIT);
        jtf_THRESHOLD_MAX.setText(Constants.THRESHOLD_MAX +"");
    }
    
    private void saveConfig() {
        Config config = new Config();
        config.PICTURE_REFRESH_TIME = Integer.valueOf(jtf_PICTURE_REFRESH_TIME.getText());
        config.HAND_THRESHOLD = Integer.valueOf(jtf_HAND_THRESHOLD.getText());
        config.HAND_MOVE_MIN = Integer.valueOf(jtf_HAND_MOVE_MIN.getText());
        config.HAND_MOVE_MAX = Integer.valueOf(jtf_HAND_MOVE_MAX.getText());
        config.PIXEL_COUNT_TRACK_MIN = Integer.valueOf(jtf_PIXEL_COUNT_TRACK_MIN.getText());
        config.PIXEL_COUNT_THRESHOLD_MIN = Integer.valueOf(jtf_PIXEL_COUNT_THRESHOLD_MIN.getText());
        config.PIXEL_COUNT_THRESHOLD_MAX = Integer.valueOf(jtf_PIXEL_COUNT_THRESHOLD_MAX.getText());
        config.YES_DELAY = Integer.valueOf(jtf_YES_DELAY.getText());
        config.PIXEL_HEIGHT = Integer.valueOf(jtf_PIXEL_HEIGHT.getText());
        config.PIXEL_WIDTH = Integer.valueOf(jtf_PIXEL_WIDTH.getText());
        config.LOAD_INFO_KINECT_IN_USE_DELAY = Integer.valueOf(jtf_LOAD_INFO_KINECT_IN_USE_DELAY.getText());
        config.ONLY_ONE_BLOB = jcb_OnlyOneBlob.isSelected();
        config.HELP_MSG = jcb_HelpMsg.isSelected();
        config.DEBUG = jcb_Debug.isSelected();
        config.HAND_TRACKER = jcb_HankTracker.isSelected();
        config.AUTOMATIC_INIT = jcb_AutomaticInit.isSelected();
        config.THRESHOLD_MAX = Integer.valueOf(jtf_THRESHOLD_MAX.getText());
        edu.utils.Constants.saveConstants(config);
    }
    
    private void loadDefaultConfig(){
        edu.utils.Constants.loadDefaultConstants();
        loadConfig();
    }
    
       
    public void loadInfoKinectInUse(InfoKinectInUse infoKinectInUse){
        this.jtf_PICTURE_REFRESH_TIME_Info.setText(infoKinectInUse.PICTURE_REFRESH_TIME + "");
        this.jtf_HAND_THRESHOLD_X_Info.setText(infoKinectInUse.HAND_THRESHOLD_X + "");
        this.jtf_HAND_THRESHOLD_Y_Info.setText(infoKinectInUse.HAND_THRESHOLD_Y + "");
        this.jtf_HAND_MOVE_Info.setText(infoKinectInUse.HAND_MOVE + "");
        this.jtf_PIXEL_COUNT_TRACK_Info.setText(infoKinectInUse.PIXEL_COUNT_TRACK + "");
        this.jtf_PIXEL_COUNT_THRESHOLD_Info.setText(infoKinectInUse.PIXEL_COUNT_THRESHOLD + "");
        this.jtf_YES_DELAY_Info.setText(infoKinectInUse.YES_DELAY + "");
        this.jtf_PIXEL_HEIGHT_Info.setText(infoKinectInUse.PIXEL_HEIGHT + "");
        this.jtf_PIXEL_WIDTH_Info.setText(infoKinectInUse.PIXEL_WIDTH + "");
        this.jtf_BLOBS_NUMBER_Info.setText(infoKinectInUse.BLOBS_NUMBER + "");
        this.jtf_FRAME_RATE_Info.setText(infoKinectInUse.FRAME_RATE + "");
        this.jtf_THRESHOLD_Info.setText(infoKinectInUse.THRESHOLD + "");
        this.jtf_TILT_Info.setText(js_tilt.getValue()+"");
    }
    
    public void setPixelCountThresholdMinMax(int min, int max){
        jtf_PIXEL_COUNT_THRESHOLD_MIN.setText(min + "");
        jtf_PIXEL_COUNT_THRESHOLD_MAX.setText(max + "");
    }
    
    private void loadPictures(){
        File file = new File("pictures");
        if(!file.exists()){
            LogReport.insertLog("A pasta 'pictures' n�o foi encontrada, ent�o n�o ser�o carregadas as imagens.", null, false);
            Constants.AUTOMATIC_INIT = false;
            return;
        }
        File[] files = file.listFiles();
            for (File picture : files) {
                if(!list.contains(picture)){
                    list.add(picture);
                }
            }
            refreshFileTable();
    }

    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jfc_ListaArquivo = new javax.swing.JFileChooser();
        jp_North = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jp_Center = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jp_Kinect = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jPanel22 = new javax.swing.JPanel();
        jl_SO = new javax.swing.JLabel();
        jPanel30 = new javax.swing.JPanel();
        jb_onOff = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jPanel23 = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        js_tilt = new javax.swing.JSlider();
        jPanel8 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jb_PastaApresentacao = new javax.swing.JButton();
        jPanel12 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jt_File = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jcb_Infinito = new javax.swing.JCheckBox();
        jLabel2 = new javax.swing.JLabel();
        js_Segundos = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jp_Configuracao = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        jtf_PICTURE_REFRESH_TIME = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jPanel16 = new javax.swing.JPanel();
        jtf_HAND_THRESHOLD = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        jtf_HAND_MOVE_MIN = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jPanel18 = new javax.swing.JPanel();
        jtf_HAND_MOVE_MAX = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jPanel20 = new javax.swing.JPanel();
        jtf_PIXEL_COUNT_TRACK_MIN = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jPanel21 = new javax.swing.JPanel();
        jtf_PIXEL_COUNT_THRESHOLD_MIN = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jPanel25 = new javax.swing.JPanel();
        jtf_PIXEL_COUNT_THRESHOLD_MAX = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jPanel27 = new javax.swing.JPanel();
        jtf_YES_DELAY = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jPanel28 = new javax.swing.JPanel();
        jtf_PIXEL_HEIGHT = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jPanel45 = new javax.swing.JPanel();
        jtf_PIXEL_WIDTH = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jPanel33 = new javax.swing.JPanel();
        jtf_LOAD_INFO_KINECT_IN_USE_DELAY = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jPanel49 = new javax.swing.JPanel();
        jtf_THRESHOLD_MAX = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        jPanel46 = new javax.swing.JPanel();
        jcb_OnlyOneBlob = new javax.swing.JCheckBox();
        jcb_HelpMsg = new javax.swing.JCheckBox();
        jcb_Debug = new javax.swing.JCheckBox();
        jPanel48 = new javax.swing.JPanel();
        jcb_HankTracker = new javax.swing.JCheckBox();
        jcb_AutomaticInit = new javax.swing.JCheckBox();
        jPanel24 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jb_Salvar = new javax.swing.JButton();
        jb_CarregarPadrao = new javax.swing.JButton();
        jp_Configuracao1 = new javax.swing.JPanel();
        jPanel29 = new javax.swing.JPanel();
        jPanel31 = new javax.swing.JPanel();
        jtf_PICTURE_REFRESH_TIME_Info = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jPanel32 = new javax.swing.JPanel();
        jtf_HAND_THRESHOLD_X_Info = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jPanel37 = new javax.swing.JPanel();
        jtf_HAND_THRESHOLD_Y_Info = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jPanel34 = new javax.swing.JPanel();
        jtf_HAND_MOVE_Info = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jPanel35 = new javax.swing.JPanel();
        jtf_PIXEL_COUNT_TRACK_Info = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jPanel36 = new javax.swing.JPanel();
        jtf_PIXEL_COUNT_THRESHOLD_Info = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jPanel38 = new javax.swing.JPanel();
        jtf_YES_DELAY_Info = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jPanel39 = new javax.swing.JPanel();
        jtf_PIXEL_HEIGHT_Info = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jPanel44 = new javax.swing.JPanel();
        jtf_PIXEL_WIDTH_Info = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jPanel47 = new javax.swing.JPanel();
        jtf_BLOBS_NUMBER_Info = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        jPanel41 = new javax.swing.JPanel();
        jtf_FRAME_RATE_Info = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        jPanel42 = new javax.swing.JPanel();
        jtf_THRESHOLD_Info = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jPanel43 = new javax.swing.JPanel();
        jtf_TILT_Info = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        jb_onOff1 = new javax.swing.JButton();
        jPanel40 = new javax.swing.JPanel();
        jp_South = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Slide Kinect");
        setMinimumSize(new java.awt.Dimension(711, 415));
        setResizable(false);

        jp_North.setBackground(new java.awt.Color(208, 223, 232));

        jLabel1.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel1.setText("Slide Kinect");
        jp_North.add(jLabel1);

        getContentPane().add(jp_North, java.awt.BorderLayout.NORTH);

        jp_Center.setBackground(new java.awt.Color(255, 255, 255));
        jp_Center.setLayout(new javax.swing.BoxLayout(jp_Center, javax.swing.BoxLayout.PAGE_AXIS));

        jp_Kinect.setBackground(new java.awt.Color(255, 255, 255));
        jp_Kinect.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Trebuchet MS", 0, 16))); // NOI18N
        jp_Kinect.setLayout(new javax.swing.BoxLayout(jp_Kinect, javax.swing.BoxLayout.LINE_AXIS));

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Kinect", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Trebuchet MS", 1, 14))); // NOI18N
        jPanel7.setMinimumSize(new java.awt.Dimension(250, 111));
        jPanel7.setPreferredSize(new java.awt.Dimension(450, 95));
        jPanel7.setLayout(new javax.swing.BoxLayout(jPanel7, javax.swing.BoxLayout.LINE_AXIS));

        jPanel9.setPreferredSize(new java.awt.Dimension(150, 110));
        jPanel9.setLayout(new javax.swing.BoxLayout(jPanel9, javax.swing.BoxLayout.PAGE_AXIS));

        jPanel22.setBackground(new java.awt.Color(255, 255, 255));
        jPanel22.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Sistema Operacional", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Trebuchet MS", 1, 14))); // NOI18N

        jl_SO.setFont(new java.awt.Font("Trebuchet MS", 0, 14)); // NOI18N
        jl_SO.setText("Windows 64");
        jPanel22.add(jl_SO);

        jPanel9.add(jPanel22);

        jPanel30.setBackground(new java.awt.Color(255, 255, 255));
        jPanel30.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Conectado", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Trebuchet MS", 1, 14))); // NOI18N

        jb_onOff.setBackground(new java.awt.Color(255, 255, 255));
        jb_onOff.setFont(new java.awt.Font("Trebuchet MS", 0, 14)); // NOI18N
        jb_onOff.setForeground(new java.awt.Color(226, 41, 50));
        jb_onOff.setIcon(new javax.swing.ImageIcon(getClass().getResource("/edu/img/turn on.png"))); // NOI18N
        jb_onOff.setText("N�o");
        jb_onOff.setPreferredSize(new java.awt.Dimension(90, 23));
        jb_onOff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jb_onOffActionPerformed(evt);
            }
        });
        jPanel30.add(jb_onOff);

        jPanel9.add(jPanel30);

        jPanel3.setBackground(new java.awt.Color(254, 254, 254));
        jPanel3.setLayout(new java.awt.BorderLayout());
        jPanel9.add(jPanel3);

        jPanel7.add(jPanel9);

        jPanel10.setPreferredSize(new java.awt.Dimension(20, 40));
        jPanel10.setLayout(new javax.swing.BoxLayout(jPanel10, javax.swing.BoxLayout.PAGE_AXIS));

        jPanel23.setBackground(new java.awt.Color(255, 255, 255));
        jPanel23.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Tilt", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Trebuchet MS", 1, 14))); // NOI18N
        jPanel23.setMinimumSize(new java.awt.Dimension(71, 40));
        jPanel23.setPreferredSize(new java.awt.Dimension(40, 200));
        jPanel23.setLayout(new javax.swing.BoxLayout(jPanel23, javax.swing.BoxLayout.PAGE_AXIS));

        jPanel19.setBackground(new java.awt.Color(255, 255, 255));
        jPanel19.setPreferredSize(new java.awt.Dimension(10, 400));
        jPanel19.setLayout(new java.awt.BorderLayout());

        js_tilt.setBackground(new java.awt.Color(255, 255, 255));
        js_tilt.setForeground(new java.awt.Color(255, 255, 255));
        js_tilt.setMaximum(30);
        js_tilt.setMinimum(-30);
        js_tilt.setOrientation(javax.swing.JSlider.VERTICAL);
        js_tilt.setValue(0);
        js_tilt.setPreferredSize(new java.awt.Dimension(10, 400));
        js_tilt.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                js_tiltStateChanged(evt);
            }
        });
        jPanel19.add(js_tilt, java.awt.BorderLayout.CENTER);

        jPanel23.add(jPanel19);

        jPanel10.add(jPanel23);

        jPanel7.add(jPanel10);

        jp_Kinect.add(jPanel7);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Slide", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Trebuchet MS", 1, 14))); // NOI18N
        jPanel8.setPreferredSize(new java.awt.Dimension(876, 88));
        jPanel8.setLayout(new javax.swing.BoxLayout(jPanel8, javax.swing.BoxLayout.PAGE_AXIS));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setPreferredSize(new java.awt.Dimension(477, 500));
        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.PAGE_AXIS));

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));

        jb_PastaApresentacao.setBackground(new java.awt.Color(255, 255, 255));
        jb_PastaApresentacao.setFont(new java.awt.Font("Trebuchet MS", 1, 14)); // NOI18N
        jb_PastaApresentacao.setText("Selecionar as Imagens para Apresenta��o");
        jb_PastaApresentacao.setMaximumSize(new java.awt.Dimension(400, 25));
        jb_PastaApresentacao.setMinimumSize(new java.awt.Dimension(0, 0));
        jb_PastaApresentacao.setPreferredSize(new java.awt.Dimension(450, 25));
        jb_PastaApresentacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jb_PastaApresentacaoActionPerformed(evt);
            }
        });
        jPanel11.add(jb_PastaApresentacao);

        jPanel1.add(jPanel11);

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.setLayout(new java.awt.BorderLayout());

        jPanel6.setBackground(new java.awt.Color(254, 254, 254));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Lista de Arquivos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Trebuchet MS", 1, 14))); // NOI18N
        jPanel6.setPreferredSize(new java.awt.Dimension(500, 300));
        jPanel6.setLayout(new javax.swing.BoxLayout(jPanel6, javax.swing.BoxLayout.LINE_AXIS));

        jt_File.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jt_File.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jt_FileMouseClicked(evt);
            }
        });
        jt_File.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jt_FileKeyTyped(evt);
            }
        });
        jScrollPane2.setViewportView(jt_File);

        jPanel6.add(jScrollPane2);

        jPanel12.add(jPanel6, java.awt.BorderLayout.CENTER);

        jPanel1.add(jPanel12);

        jPanel8.add(jPanel1);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jButton1.setBackground(new java.awt.Color(255, 255, 255));
        jButton1.setFont(new java.awt.Font("Trebuchet MS", 1, 14)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/edu/img/play.png"))); // NOI18N
        jButton1.setText("Slide");
        jButton1.setPreferredSize(new java.awt.Dimension(110, 25));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton1);

        jcb_Infinito.setBackground(new java.awt.Color(255, 255, 255));
        jcb_Infinito.setFont(new java.awt.Font("Trebuchet MS", 0, 14)); // NOI18N
        jcb_Infinito.setSelected(true);
        jcb_Infinito.setText("Infinito");
        jcb_Infinito.setPreferredSize(new java.awt.Dimension(100, 23));
        jPanel4.add(jcb_Infinito);

        jLabel2.setText("Tempo entre trocas de");
        jPanel4.add(jLabel2);

        js_Segundos.setPreferredSize(new java.awt.Dimension(60, 20));
        jPanel4.add(js_Segundos);

        jLabel3.setText("segundos");
        jPanel4.add(jLabel3);

        jPanel8.add(jPanel4);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));
        jPanel8.add(jPanel5);

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.setLayout(new java.awt.BorderLayout());
        jPanel8.add(jPanel13);

        jp_Kinect.add(jPanel8);

        jTabbedPane1.addTab("Kinect", jp_Kinect);

        jp_Configuracao.setBackground(new java.awt.Color(254, 254, 254));
        jp_Configuracao.setLayout(new javax.swing.BoxLayout(jp_Configuracao, javax.swing.BoxLayout.PAGE_AXIS));

        jPanel2.setLayout(new javax.swing.BoxLayout(jPanel2, javax.swing.BoxLayout.PAGE_AXIS));

        jPanel15.setBackground(new java.awt.Color(254, 254, 254));
        jPanel15.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_PICTURE_REFRESH_TIME.setFont(new java.awt.Font("Tlwg Typist", 1, 20)); // NOI18N
        jtf_PICTURE_REFRESH_TIME.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel15.add(jtf_PICTURE_REFRESH_TIME);

        jLabel4.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel4.setText("Tempo entre troca de imagens pela m�o (Frame).");
        jLabel4.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel15.add(jLabel4);

        jPanel2.add(jPanel15);

        jPanel16.setBackground(new java.awt.Color(254, 254, 254));
        jPanel16.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_HAND_THRESHOLD.setFont(new java.awt.Font("Tlwg Typist", 1, 20)); // NOI18N
        jtf_HAND_THRESHOLD.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel16.add(jtf_HAND_THRESHOLD);

        jLabel5.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel5.setText("Distancia m�xima entre o centro do maior Blob e o centro da soma de todos os Blobs (Pixel).");
        jLabel5.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel16.add(jLabel5);

        jPanel2.add(jPanel16);

        jPanel17.setBackground(new java.awt.Color(254, 254, 254));
        jPanel17.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_HAND_MOVE_MIN.setFont(new java.awt.Font("Tlwg Typist", 1, 20)); // NOI18N
        jtf_HAND_MOVE_MIN.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel17.add(jtf_HAND_MOVE_MIN);

        jLabel6.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel6.setText("Dist�ncia m�nima entre o in�cio e o fim do movimento da m�o para efetuar a troca de Slides (Pixel).");
        jLabel6.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel17.add(jLabel6);

        jPanel2.add(jPanel17);

        jPanel18.setBackground(new java.awt.Color(254, 254, 254));
        jPanel18.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_HAND_MOVE_MAX.setFont(new java.awt.Font("Tlwg Typist", 1, 20)); // NOI18N
        jtf_HAND_MOVE_MAX.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel18.add(jtf_HAND_MOVE_MAX);

        jLabel7.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel7.setText("Dist�ncia m�xima entre o in�cio e o fim do movimento da m�o para efetuar a troca de Slides (Pixel).");
        jLabel7.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel18.add(jLabel7);

        jPanel2.add(jPanel18);

        jPanel20.setBackground(new java.awt.Color(254, 254, 254));
        jPanel20.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_PIXEL_COUNT_TRACK_MIN.setFont(new java.awt.Font("Tlwg Typist", 1, 20)); // NOI18N
        jtf_PIXEL_COUNT_TRACK_MIN.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel20.add(jtf_PIXEL_COUNT_TRACK_MIN);

        jLabel8.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel8.setText("Tamanho m�nimo do maior Blob (Pixel).");
        jLabel8.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel20.add(jLabel8);

        jPanel2.add(jPanel20);

        jPanel21.setBackground(new java.awt.Color(254, 254, 254));
        jPanel21.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_PIXEL_COUNT_THRESHOLD_MIN.setFont(new java.awt.Font("Tlwg Typist", 1, 20)); // NOI18N
        jtf_PIXEL_COUNT_THRESHOLD_MIN.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel21.add(jtf_PIXEL_COUNT_THRESHOLD_MIN);

        jLabel9.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel9.setText("Tamanho m�nimo do maior Blob para identificar a m�o do usu�rio (Pixel).");
        jLabel9.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel21.add(jLabel9);

        jPanel2.add(jPanel21);

        jPanel25.setBackground(new java.awt.Color(254, 254, 254));
        jPanel25.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_PIXEL_COUNT_THRESHOLD_MAX.setFont(new java.awt.Font("Tlwg Typist", 1, 20)); // NOI18N
        jtf_PIXEL_COUNT_THRESHOLD_MAX.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel25.add(jtf_PIXEL_COUNT_THRESHOLD_MAX);

        jLabel10.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel10.setText("Tamanho m�ximo do maior Blob para identificar a m�o do usu�rio (Pixel).");
        jLabel10.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel25.add(jLabel10);

        jPanel2.add(jPanel25);

        jPanel27.setBackground(new java.awt.Color(254, 254, 254));
        jPanel27.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_YES_DELAY.setFont(new java.awt.Font("Tlwg Typist", 1, 20)); // NOI18N
        jtf_YES_DELAY.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel27.add(jtf_YES_DELAY);

        jLabel11.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel11.setText("Delay para troca entre status de 'SIM' (Verde) e 'N�O' (Vermelho) (Frame).");
        jLabel11.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel27.add(jLabel11);

        jPanel2.add(jPanel27);

        jPanel28.setBackground(new java.awt.Color(254, 254, 254));
        jPanel28.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_PIXEL_HEIGHT.setFont(new java.awt.Font("Tlwg Typist", 1, 20)); // NOI18N
        jtf_PIXEL_HEIGHT.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel28.add(jtf_PIXEL_HEIGHT);

        jLabel12.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel12.setText("Altura m�xima do maior Blob (Pixel).");
        jLabel12.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel28.add(jLabel12);

        jPanel2.add(jPanel28);

        jPanel45.setBackground(new java.awt.Color(254, 254, 254));
        jPanel45.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_PIXEL_WIDTH.setFont(new java.awt.Font("Tlwg Typist", 1, 20)); // NOI18N
        jtf_PIXEL_WIDTH.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel45.add(jtf_PIXEL_WIDTH);

        jLabel26.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel26.setText("Largura m�xima do maior Blob (Pixel).");
        jLabel26.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel45.add(jLabel26);

        jPanel2.add(jPanel45);

        jPanel33.setBackground(new java.awt.Color(254, 254, 254));
        jPanel33.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_LOAD_INFO_KINECT_IN_USE_DELAY.setFont(new java.awt.Font("Tlwg Typist", 1, 20)); // NOI18N
        jtf_LOAD_INFO_KINECT_IN_USE_DELAY.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel33.add(jtf_LOAD_INFO_KINECT_IN_USE_DELAY);

        jLabel15.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel15.setText("Delay entre cada atualiza��o das informa��es do Kinect em Uso");
        jLabel15.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel33.add(jLabel15);

        jPanel2.add(jPanel33);

        jPanel49.setBackground(new java.awt.Color(254, 254, 254));
        jPanel49.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_THRESHOLD_MAX.setFont(new java.awt.Font("Tlwg Typist", 1, 20)); // NOI18N
        jtf_THRESHOLD_MAX.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel49.add(jtf_THRESHOLD_MAX);

        jLabel28.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel28.setText("Threshold M�ximo");
        jLabel28.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel49.add(jLabel28);

        jPanel2.add(jPanel49);

        jPanel46.setBackground(new java.awt.Color(254, 254, 254));
        jPanel46.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jcb_OnlyOneBlob.setBackground(new java.awt.Color(255, 255, 255));
        jcb_OnlyOneBlob.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jcb_OnlyOneBlob.setText("Manter apenas um Blob focado");
        jcb_OnlyOneBlob.setPreferredSize(new java.awt.Dimension(280, 23));
        jPanel46.add(jcb_OnlyOneBlob);

        jcb_HelpMsg.setBackground(new java.awt.Color(255, 255, 255));
        jcb_HelpMsg.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jcb_HelpMsg.setText("Mostrar mensagem de ajuda");
        jcb_HelpMsg.setPreferredSize(new java.awt.Dimension(280, 23));
        jPanel46.add(jcb_HelpMsg);

        jcb_Debug.setBackground(new java.awt.Color(255, 255, 255));
        jcb_Debug.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jcb_Debug.setText("Depurar (Apresenta o Kinect a cada 30 Frames)");
        jcb_Debug.setPreferredSize(new java.awt.Dimension(400, 23));
        jPanel46.add(jcb_Debug);

        jPanel2.add(jPanel46);

        jPanel48.setBackground(new java.awt.Color(254, 254, 254));
        jPanel48.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jcb_HankTracker.setBackground(new java.awt.Color(255, 255, 255));
        jcb_HankTracker.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jcb_HankTracker.setText("Hand Tracker");
        jcb_HankTracker.setPreferredSize(new java.awt.Dimension(280, 23));
        jPanel48.add(jcb_HankTracker);

        jcb_AutomaticInit.setBackground(new java.awt.Color(255, 255, 255));
        jcb_AutomaticInit.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jcb_AutomaticInit.setText("Iniciar Automaticamente");
        jcb_AutomaticInit.setPreferredSize(new java.awt.Dimension(280, 23));
        jPanel48.add(jcb_AutomaticInit);

        jPanel2.add(jPanel48);

        jPanel24.setBackground(new java.awt.Color(254, 254, 254));
        jPanel24.setLayout(new java.awt.BorderLayout());
        jPanel2.add(jPanel24);

        jp_Configuracao.add(jPanel2);

        jPanel14.setBackground(new java.awt.Color(254, 254, 254));
        jPanel14.setBorder(null);

        jb_Salvar.setBackground(new java.awt.Color(255, 255, 255));
        jb_Salvar.setFont(new java.awt.Font("Trebuchet MS", 1, 14)); // NOI18N
        jb_Salvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/edu/img/ok.png"))); // NOI18N
        jb_Salvar.setMnemonic('s');
        jb_Salvar.setText("Salvar");
        jb_Salvar.setMaximumSize(new java.awt.Dimension(400, 25));
        jb_Salvar.setMinimumSize(new java.awt.Dimension(0, 0));
        jb_Salvar.setPreferredSize(new java.awt.Dimension(150, 25));
        jb_Salvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jb_SalvarActionPerformed(evt);
            }
        });
        jPanel14.add(jb_Salvar);

        jb_CarregarPadrao.setBackground(new java.awt.Color(255, 255, 255));
        jb_CarregarPadrao.setFont(new java.awt.Font("Trebuchet MS", 1, 14)); // NOI18N
        jb_CarregarPadrao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/edu/img/ok.png"))); // NOI18N
        jb_CarregarPadrao.setMnemonic('c');
        jb_CarregarPadrao.setText("Carregar Padr�o");
        jb_CarregarPadrao.setMaximumSize(new java.awt.Dimension(400, 25));
        jb_CarregarPadrao.setMinimumSize(new java.awt.Dimension(0, 0));
        jb_CarregarPadrao.setPreferredSize(new java.awt.Dimension(250, 25));
        jb_CarregarPadrao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jb_CarregarPadraoActionPerformed(evt);
            }
        });
        jPanel14.add(jb_CarregarPadrao);

        jp_Configuracao.add(jPanel14);

        jTabbedPane1.addTab("Configura��o", jp_Configuracao);

        jp_Configuracao1.setBackground(new java.awt.Color(254, 254, 254));
        jp_Configuracao1.setLayout(new javax.swing.BoxLayout(jp_Configuracao1, javax.swing.BoxLayout.PAGE_AXIS));

        jPanel29.setLayout(new javax.swing.BoxLayout(jPanel29, javax.swing.BoxLayout.PAGE_AXIS));

        jPanel31.setBackground(new java.awt.Color(254, 254, 254));
        jPanel31.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_PICTURE_REFRESH_TIME_Info.setEditable(false);
        jtf_PICTURE_REFRESH_TIME_Info.setFont(new java.awt.Font("TlwgTypewriter", 1, 20)); // NOI18N
        jtf_PICTURE_REFRESH_TIME_Info.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel31.add(jtf_PICTURE_REFRESH_TIME_Info);

        jLabel13.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel13.setText("Tempo para poder trocar a imagem pela m�o (Frame).");
        jLabel13.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel31.add(jLabel13);

        jPanel29.add(jPanel31);

        jPanel32.setBackground(new java.awt.Color(254, 254, 254));
        jPanel32.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_HAND_THRESHOLD_X_Info.setEditable(false);
        jtf_HAND_THRESHOLD_X_Info.setFont(new java.awt.Font("TlwgTypewriter", 1, 20)); // NOI18N
        jtf_HAND_THRESHOLD_X_Info.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel32.add(jtf_HAND_THRESHOLD_X_Info);

        jLabel14.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel14.setText("Distancia do eixo X entre o centro do maior Blob e o centro da soma de todos os Blobs (Pixel).");
        jLabel14.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel32.add(jLabel14);

        jPanel29.add(jPanel32);

        jPanel37.setBackground(new java.awt.Color(254, 254, 254));
        jPanel37.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_HAND_THRESHOLD_Y_Info.setEditable(false);
        jtf_HAND_THRESHOLD_Y_Info.setFont(new java.awt.Font("TlwgTypewriter", 1, 20)); // NOI18N
        jtf_HAND_THRESHOLD_Y_Info.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel37.add(jtf_HAND_THRESHOLD_Y_Info);

        jLabel19.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel19.setText("Distancia do eixo Y entre o centro do maior Blob e o centro da soma de todos os Blobs (Pixel).");
        jLabel19.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel37.add(jLabel19);

        jPanel29.add(jPanel37);

        jPanel34.setBackground(new java.awt.Color(254, 254, 254));
        jPanel34.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_HAND_MOVE_Info.setEditable(false);
        jtf_HAND_MOVE_Info.setFont(new java.awt.Font("TlwgTypewriter", 1, 20)); // NOI18N
        jtf_HAND_MOVE_Info.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel34.add(jtf_HAND_MOVE_Info);

        jLabel16.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel16.setText("Dist�ncia entre o in�cio e o fim do movimento da m�o para efetuar a troca de Slides (Pixel).");
        jLabel16.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel34.add(jLabel16);

        jPanel29.add(jPanel34);

        jPanel35.setBackground(new java.awt.Color(254, 254, 254));
        jPanel35.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_PIXEL_COUNT_TRACK_Info.setEditable(false);
        jtf_PIXEL_COUNT_TRACK_Info.setFont(new java.awt.Font("TlwgTypewriter", 1, 20)); // NOI18N
        jtf_PIXEL_COUNT_TRACK_Info.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel35.add(jtf_PIXEL_COUNT_TRACK_Info);

        jLabel17.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel17.setText("Tamanho da soma de todos os Blobs (Pixel).");
        jLabel17.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel35.add(jLabel17);

        jPanel29.add(jPanel35);

        jPanel36.setBackground(new java.awt.Color(254, 254, 254));
        jPanel36.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_PIXEL_COUNT_THRESHOLD_Info.setEditable(false);
        jtf_PIXEL_COUNT_THRESHOLD_Info.setFont(new java.awt.Font("TlwgTypewriter", 1, 20)); // NOI18N
        jtf_PIXEL_COUNT_THRESHOLD_Info.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel36.add(jtf_PIXEL_COUNT_THRESHOLD_Info);

        jLabel18.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel18.setText("Tamanho do maior Blob indentificado como a m�o do usu�rio (Pixel).");
        jLabel18.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel36.add(jLabel18);

        jPanel29.add(jPanel36);

        jPanel38.setBackground(new java.awt.Color(254, 254, 254));
        jPanel38.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_YES_DELAY_Info.setEditable(false);
        jtf_YES_DELAY_Info.setFont(new java.awt.Font("TlwgTypewriter", 1, 20)); // NOI18N
        jtf_YES_DELAY_Info.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel38.add(jtf_YES_DELAY_Info);

        jLabel20.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel20.setText("Tempo para a troca do status de 'SIM' (Verde) e 'N�O' (Vermelho) (Frame).");
        jLabel20.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel38.add(jLabel20);

        jPanel29.add(jPanel38);

        jPanel39.setBackground(new java.awt.Color(254, 254, 254));
        jPanel39.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_PIXEL_HEIGHT_Info.setEditable(false);
        jtf_PIXEL_HEIGHT_Info.setFont(new java.awt.Font("TlwgTypewriter", 1, 20)); // NOI18N
        jtf_PIXEL_HEIGHT_Info.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel39.add(jtf_PIXEL_HEIGHT_Info);

        jLabel21.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel21.setText("Altura do maior Blob (Pixel).");
        jLabel21.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel39.add(jLabel21);

        jPanel29.add(jPanel39);

        jPanel44.setBackground(new java.awt.Color(254, 254, 254));
        jPanel44.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_PIXEL_WIDTH_Info.setEditable(false);
        jtf_PIXEL_WIDTH_Info.setFont(new java.awt.Font("TlwgTypewriter", 1, 20)); // NOI18N
        jtf_PIXEL_WIDTH_Info.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel44.add(jtf_PIXEL_WIDTH_Info);

        jLabel25.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel25.setText("Largura do maior Blob (Pixel).");
        jLabel25.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel44.add(jLabel25);

        jPanel29.add(jPanel44);

        jPanel47.setBackground(new java.awt.Color(254, 254, 254));
        jPanel47.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_BLOBS_NUMBER_Info.setEditable(false);
        jtf_BLOBS_NUMBER_Info.setFont(new java.awt.Font("TlwgTypewriter", 1, 20)); // NOI18N
        jtf_BLOBS_NUMBER_Info.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel47.add(jtf_BLOBS_NUMBER_Info);

        jLabel27.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel27.setText("N�meros de Blobs");
        jLabel27.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel47.add(jLabel27);

        jPanel29.add(jPanel47);

        jPanel41.setBackground(new java.awt.Color(254, 254, 254));
        jPanel41.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_FRAME_RATE_Info.setEditable(false);
        jtf_FRAME_RATE_Info.setFont(new java.awt.Font("TlwgTypewriter", 1, 20)); // NOI18N
        jtf_FRAME_RATE_Info.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel41.add(jtf_FRAME_RATE_Info);

        jLabel22.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel22.setText("Kinect Frame Rate");
        jLabel22.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel41.add(jLabel22);

        jPanel29.add(jPanel41);

        jPanel42.setBackground(new java.awt.Color(254, 254, 254));
        jPanel42.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_THRESHOLD_Info.setEditable(false);
        jtf_THRESHOLD_Info.setFont(new java.awt.Font("TlwgTypewriter", 1, 20)); // NOI18N
        jtf_THRESHOLD_Info.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel42.add(jtf_THRESHOLD_Info);

        jLabel23.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel23.setText("Kinect Threshold");
        jLabel23.setPreferredSize(new java.awt.Dimension(800, 20));
        jPanel42.add(jLabel23);

        jPanel29.add(jPanel42);

        jPanel43.setBackground(new java.awt.Color(254, 254, 254));
        jPanel43.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jtf_TILT_Info.setEditable(false);
        jtf_TILT_Info.setFont(new java.awt.Font("TlwgTypewriter", 1, 20)); // NOI18N
        jtf_TILT_Info.setPreferredSize(new java.awt.Dimension(100, 27));
        jPanel43.add(jtf_TILT_Info);

        jLabel24.setFont(new java.awt.Font("TakaoPGothic", 1, 14)); // NOI18N
        jLabel24.setText("Kinect Tilt");
        jLabel24.setPreferredSize(new java.awt.Dimension(100, 20));
        jPanel43.add(jLabel24);

        jb_onOff1.setBackground(new java.awt.Color(255, 255, 255));
        jb_onOff1.setFont(new java.awt.Font("Trebuchet MS", 0, 14)); // NOI18N
        jb_onOff1.setForeground(new java.awt.Color(1, 1, 1));
        jb_onOff1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/edu/img/config.png"))); // NOI18N
        jb_onOff1.setText("Resetar Tilt");
        jb_onOff1.setPreferredSize(new java.awt.Dimension(170, 23));
        jb_onOff1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jb_onOff1ActionPerformed(evt);
            }
        });
        jPanel43.add(jb_onOff1);

        jPanel29.add(jPanel43);

        jPanel40.setBackground(new java.awt.Color(254, 254, 254));
        jPanel40.setLayout(new java.awt.BorderLayout());
        jPanel29.add(jPanel40);

        jp_Configuracao1.add(jPanel29);

        jTabbedPane1.addTab("Informa��es do Kinect Em Uso", jp_Configuracao1);

        jp_Center.add(jTabbedPane1);

        getContentPane().add(jp_Center, java.awt.BorderLayout.CENTER);

        jp_South.setBackground(new java.awt.Color(208, 223, 232));
        jp_South.setPreferredSize(new java.awt.Dimension(668, 35));
        getContentPane().add(jp_South, java.awt.BorderLayout.PAGE_END);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jb_onOffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jb_onOffActionPerformed
        turnOnOff();
    }//GEN-LAST:event_jb_onOffActionPerformed

    private void js_tiltStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_js_tiltStateChanged
        tiltUpDown(js_tilt.getValue());
    }//GEN-LAST:event_js_tiltStateChanged

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        slideShow();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jt_FileMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jt_FileMouseClicked
      
    }//GEN-LAST:event_jt_FileMouseClicked

    private void jt_FileKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jt_FileKeyTyped
        keyTypedAtList(evt);
    }//GEN-LAST:event_jt_FileKeyTyped

    private void jb_PastaApresentacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jb_PastaApresentacaoActionPerformed
        addImages();
    }//GEN-LAST:event_jb_PastaApresentacaoActionPerformed

    private void jb_SalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jb_SalvarActionPerformed
        saveConfig();
    }//GEN-LAST:event_jb_SalvarActionPerformed

    private void jb_CarregarPadraoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jb_CarregarPadraoActionPerformed
        loadDefaultConfig();
    }//GEN-LAST:event_jb_CarregarPadraoActionPerformed

    private void jb_onOff1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jb_onOff1ActionPerformed
        js_tilt.setValue(0);
    }//GEN-LAST:event_jb_onOff1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel27;
    private javax.swing.JPanel jPanel28;
    private javax.swing.JPanel jPanel29;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel30;
    private javax.swing.JPanel jPanel31;
    private javax.swing.JPanel jPanel32;
    private javax.swing.JPanel jPanel33;
    private javax.swing.JPanel jPanel34;
    private javax.swing.JPanel jPanel35;
    private javax.swing.JPanel jPanel36;
    private javax.swing.JPanel jPanel37;
    private javax.swing.JPanel jPanel38;
    private javax.swing.JPanel jPanel39;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel40;
    private javax.swing.JPanel jPanel41;
    private javax.swing.JPanel jPanel42;
    private javax.swing.JPanel jPanel43;
    private javax.swing.JPanel jPanel44;
    private javax.swing.JPanel jPanel45;
    private javax.swing.JPanel jPanel46;
    private javax.swing.JPanel jPanel47;
    private javax.swing.JPanel jPanel48;
    private javax.swing.JPanel jPanel49;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JButton jb_CarregarPadrao;
    private javax.swing.JButton jb_PastaApresentacao;
    private javax.swing.JButton jb_Salvar;
    private javax.swing.JButton jb_onOff;
    private javax.swing.JButton jb_onOff1;
    private javax.swing.JCheckBox jcb_AutomaticInit;
    private javax.swing.JCheckBox jcb_Debug;
    private javax.swing.JCheckBox jcb_HankTracker;
    private javax.swing.JCheckBox jcb_HelpMsg;
    private javax.swing.JCheckBox jcb_Infinito;
    private javax.swing.JCheckBox jcb_OnlyOneBlob;
    private javax.swing.JFileChooser jfc_ListaArquivo;
    private javax.swing.JLabel jl_SO;
    private javax.swing.JPanel jp_Center;
    private javax.swing.JPanel jp_Configuracao;
    private javax.swing.JPanel jp_Configuracao1;
    private javax.swing.JPanel jp_Kinect;
    private javax.swing.JPanel jp_North;
    private javax.swing.JPanel jp_South;
    private javax.swing.JSpinner js_Segundos;
    private javax.swing.JSlider js_tilt;
    private javax.swing.JTable jt_File;
    private javax.swing.JTextField jtf_BLOBS_NUMBER_Info;
    private javax.swing.JTextField jtf_FRAME_RATE_Info;
    private javax.swing.JTextField jtf_HAND_MOVE_Info;
    private javax.swing.JTextField jtf_HAND_MOVE_MAX;
    private javax.swing.JTextField jtf_HAND_MOVE_MIN;
    private javax.swing.JTextField jtf_HAND_THRESHOLD;
    private javax.swing.JTextField jtf_HAND_THRESHOLD_X_Info;
    private javax.swing.JTextField jtf_HAND_THRESHOLD_Y_Info;
    private javax.swing.JTextField jtf_LOAD_INFO_KINECT_IN_USE_DELAY;
    private javax.swing.JTextField jtf_PICTURE_REFRESH_TIME;
    private javax.swing.JTextField jtf_PICTURE_REFRESH_TIME_Info;
    private javax.swing.JTextField jtf_PIXEL_COUNT_THRESHOLD_Info;
    private javax.swing.JTextField jtf_PIXEL_COUNT_THRESHOLD_MAX;
    private javax.swing.JTextField jtf_PIXEL_COUNT_THRESHOLD_MIN;
    private javax.swing.JTextField jtf_PIXEL_COUNT_TRACK_Info;
    private javax.swing.JTextField jtf_PIXEL_COUNT_TRACK_MIN;
    private javax.swing.JTextField jtf_PIXEL_HEIGHT;
    private javax.swing.JTextField jtf_PIXEL_HEIGHT_Info;
    private javax.swing.JTextField jtf_PIXEL_WIDTH;
    private javax.swing.JTextField jtf_PIXEL_WIDTH_Info;
    private javax.swing.JTextField jtf_THRESHOLD_Info;
    private javax.swing.JTextField jtf_THRESHOLD_MAX;
    private javax.swing.JTextField jtf_TILT_Info;
    private javax.swing.JTextField jtf_YES_DELAY;
    private javax.swing.JTextField jtf_YES_DELAY_Info;
    // End of variables declaration//GEN-END:variables
}
