/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.gui.utils;

import edu.gui.principal.JPrincipal;
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import ssu.log.LogReport;

/**
 *
 * @author Zinho
 */
public class JPicture extends javax.swing.JFrame {

    private ImageIcon actualImageIcon = null;
    private ArrayList<ImageIcon> imagesIcons = null;
    private boolean circular;
    private int time = 5;
    private int actualTime = 0;
    private Thread timeThread;    
    private JPrincipal jPrincipal;

    /**
     * Creates new form JPicture
     */
    public JPicture() {
        initComponents();
        imagesIcons = new ArrayList();
        initTimeThread();
        
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);

        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (e.getKeyChar() == KeyEvent.VK_ESCAPE) {
                    if(timeThread != null){
                        timeThread.interrupt();
                    }
                    dispose();
                    
                }
                if (e.getKeyChar() == KeyEvent.VK_MINUS) {
                    zoom(false);
                }
                if (e.getKeyChar() == KeyEvent.VK_SPACE) {
                    zoom(true);
                }
                if (e.getKeyChar() == 'A') {
                    try {
                        moveLeft();
                    } catch (Exception ex) {
                        LogReport.insertLog("Erro ao mover � esquerda.", ex, true);
                    }
                }
                if (e.getKeyChar() == 'D') {
                    try {
                        moveRight();
                    } catch (Exception ex) {
                        LogReport.insertLog("Erro ao mover � direita.", ex, true);
                    }
                }
            }
        });
    }
    
    public void moveLeft() throws Exception {
        int index = imagesIcons.indexOf(this.actualImageIcon);
        if(index != -1 && index < (imagesIcons.size()-1)){
            ImageIcon imageIcon = imagesIcons.get(++index);
            DisplayImage(imageIcon);
        } else if (this.circular){
            index = -1;
            ImageIcon imageIcon = imagesIcons.get(++index);
            DisplayImage(imageIcon);            
        }
    }
    
    public void moveRight() throws Exception {
        int index = imagesIcons.indexOf(this.actualImageIcon);
        if(index != -1 && index < imagesIcons.size() && index > 0){
            ImageIcon imageIcon = imagesIcons.get(--index);
            DisplayImage(imageIcon);
        } else if(this.circular){
            index = this.imagesIcons.size();
            ImageIcon imageIcon = imagesIcons.get(--index);
            DisplayImage(imageIcon);
        }
    }

    public void putImage(String imageIcon) throws Exception {
        BufferedImage img = ImageIO.read(new File(imageIcon));
        putImage(new ImageIcon(img));
    }

    public void putImage(ImageIcon imageIcon) throws Exception {
        this.imagesIcons.add(imageIcon);
    }

    public boolean removeImage(String imageIcon) throws Exception {
        BufferedImage img = ImageIO.read(new File(imageIcon));
        return removeImage(new ImageIcon(img));
    }

    public boolean removeImage(ImageIcon imageIcon) throws Exception {
        return this.imagesIcons.remove(imageIcon);
    }

    public void DisplayImage(ImageIcon imageIcon) throws IOException, Exception {
        this.actualImageIcon = imageIcon;
        this.jl_image.setIcon(actualImageIcon);
        repaint();
        setVisible(true);
    }
    
    
    public void initSlideShow() throws Exception{
        if(imagesIcons.isEmpty()){
            return;
        }
        DisplayImage(imagesIcons.get(0));
    }


    public void zoom(boolean in) {
        if (in) {
            resizeIcon(0.1, 0.1);
        } else {
            resizeIcon(-0.1, -0.1);

        }
    }

    private void resizeIcon(double height, double width) {
        int newHeight = Double.valueOf(actualImageIcon.getIconHeight() + (actualImageIcon.getIconHeight() * height)).intValue();
        int newWidth = Double.valueOf(actualImageIcon.getIconWidth() + (actualImageIcon.getIconWidth() * width)).intValue();
        System.out.println(newHeight + " " + newWidth);
        Image newimg = actualImageIcon.getImage().getScaledInstance(
                newWidth,
                newHeight,
                java.awt.Image.SCALE_REPLICATE); // scale it the smooth way  
        actualImageIcon = new ImageIcon(newimg);
        this.jl_image.setIcon(actualImageIcon);

    }
    
    public void setCircular(boolean circular){
        this.circular = circular;
    }
    
    public void setTime(int time){
        this.time = time;
    }
    
    public void setJPrincipal(JPrincipal jPrincipal){
        this.jPrincipal = jPrincipal;
    }
    
    private void initTimeThread(){
       timeThread =  new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    try {
                        Thread.sleep(1000);
                        actualTime++;
                        if(actualTime >= time){
                            moveLeft();
                            actualTime = 0;
                        }
                    } catch (InterruptedException ie){
                        actualTime = 0;
                        if(jPrincipal != null){
                            jPrincipal.stopKinect();
                        }
                        LogReport.insertLog("Thread interrompida.", ie, false);
                        break;
                    } catch (Exception e) {
                        LogReport.insertLog("Erro interno no temporizador", e, false);
                    }
                }
            }
        });
       timeThread.start();
    }
    
     

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jl_image = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));
        jPanel1.setLayout(new java.awt.BorderLayout());

        jl_image.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel1.add(jl_image, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel jl_image;
    // End of variables declaration//GEN-END:variables
}
