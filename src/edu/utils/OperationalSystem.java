/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utils;

/**
 *
 * @author Zinho
 */
public class OperationalSystem {
    public static final String OS_NAME = System.getProperty("os.name");
    public static final String OS_ARCH = System.getProperty("os.arch").contains("64") ? "64" : "32";
    public static final String OS_NAME_ARCH = OS_NAME + " " + OS_ARCH;
}
