/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import ssu.log.LogReport;

/**
 *
 * @author zinho
 */
public class Constants {

    public static int PICTURE_REFRESH_TIME = 60;
    public static int HAND_THRESHOLD = 30;
    public static int HAND_MOVE_MIN = 35;
    public static int HAND_MOVE_MAX = 90;
    public static int PIXEL_COUNT_TRACK_MIN = 2500;
    public static int PIXEL_COUNT_THRESHOLD_MIN = 5000;
    public static int PIXEL_COUNT_THRESHOLD_MAX = 8000;
    public static int YES_DELAY = 200;
    public static int PIXEL_HEIGHT = 180;
    public static int PIXEL_WIDTH = 220;
    public static int LOAD_INFO_KINECT_IN_USE_DELAY = 30;
    public static boolean ONLY_ONE_BLOB = false;
    public static boolean HELP_MSG = true;
    public static boolean DEBUG = false;
    public static int AUTOMATIC_SLIDE_TIME = 3600;
    public static int THRESHOLD_MAX = 800;
    public static int THRESHOLD_MIN = 500;
    public static boolean HAND_TRACKER = false;
    public static boolean AUTOMATIC_INIT = true;

    private static HashMap<String, Integer> map = null;
    
    private static Properties props;

    public static void initMap(){
         map = new HashMap();
         map.put("PICTURE_REFRESH_TIME", PICTURE_REFRESH_TIME);
         map.put("HAND_THRESHOLD", HAND_THRESHOLD);
         map.put("HAND_MOVE_MIN", HAND_MOVE_MIN);
         map.put("HAND_MOVE_MAX", HAND_MOVE_MAX);
         map.put("PIXEL_COUNT_TRACK_MIN", PIXEL_COUNT_TRACK_MIN);
         map.put("PIXEL_COUNT_THRESHOLD_MIN", PIXEL_COUNT_THRESHOLD_MIN);
         map.put("PIXEL_COUNT_THRESHOLD_MAX", PIXEL_COUNT_THRESHOLD_MAX);
         map.put("YES_DELAY", YES_DELAY);
         map.put("PIXEL_HEIGHT", PIXEL_HEIGHT);
         map.put("PIXEL_WIDTH", PIXEL_WIDTH);
         map.put("LOAD_INFO_KINECT_IN_USE_DELAY", LOAD_INFO_KINECT_IN_USE_DELAY);
         map.put("ONLY_ONE_BLOB", ONLY_ONE_BLOB ? 1 : 0);
         map.put("HELP_MSG", HELP_MSG ? 1 : 0);
         map.put("DEBUG", DEBUG ? 1 : 0);
         map.put("AUTOMATIC_SLIDE_TIME", AUTOMATIC_SLIDE_TIME);
         map.put("AUTOMATIC_INIT", AUTOMATIC_INIT ? 1 : 0);
         map.put("HAND_TRACKER", HAND_TRACKER ? 1 : 0);
         map.put("THRESHOLD_MAX", THRESHOLD_MAX);
    }
    public static void loadDefaultConstants(){
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            switch (key){
                case "PICTURE_REFRESH_TIME":
                    PICTURE_REFRESH_TIME = value;
                    break;
                case "HAND_THRESHOLD":
                    HAND_THRESHOLD = value;
                    break;
                case "HAND_MOVE_MIN":
                    HAND_MOVE_MIN = value;
                    break;
                case "HAND_MOVE_MAX":
                    HAND_MOVE_MAX = value;
                    break;
                case "PIXEL_COUNT_TRACK_MIN":
                    PIXEL_COUNT_TRACK_MIN = value;
                    break;
                case "PIXEL_COUNT_THRESHOLD_MIN":
                    PIXEL_COUNT_THRESHOLD_MIN = value;
                    break;
                case "PIXEL_COUNT_THRESHOLD_MAX":
                    PIXEL_COUNT_THRESHOLD_MAX = value;
                    break;
                case "YES_DELAY":
                    YES_DELAY = value;
                    break;
                case "PIXEL_HEIGHT":
                    PIXEL_HEIGHT = value;
                    break;
                case "PIXEL_WIDTH":
                    PIXEL_WIDTH = value;
                    break;
                case "LOAD_INFO_KINECT_IN_USE_DELAY":
                    LOAD_INFO_KINECT_IN_USE_DELAY = value;
                    break;
                case "ONLY_ONE_BLOB":
                    ONLY_ONE_BLOB = value == 1;
                    break;
                case "HELP_MSG":
                    HELP_MSG = value == 1;
                    break;
                case "DEBUG":
                    DEBUG = value == 1;
                    break;
                case "AUTOMATIC_SLIDE_TIME":
                    AUTOMATIC_SLIDE_TIME = value;
                    break;
                case "HAND_TRACKER":
                    HAND_TRACKER = value == 1;
                    break;
                case "AUTOMATIC_INIT":
                    AUTOMATIC_INIT = value == 1;
                    break;
                case "THRESHOLD_MAX":
                    THRESHOLD_MAX = value;
                    break;
                default:
                    break;                    
            }
        }
        saveConstants();
    }
    
    private static void saveConstants() {
        try {
            String constants = "constants.txt";
            File file = new File(constants);
            if (!file.exists()) {
                file.createNewFile();
            }
            
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.append("PICTURE_REFRESH_TIME = " + PICTURE_REFRESH_TIME);
            writer.newLine();
            writer.append("HAND_THRESHOLD = " + HAND_THRESHOLD);
            writer.newLine();
            writer.append("HAND_MOVE_MIN = " + HAND_MOVE_MIN);
            writer.newLine();
            writer.append("HAND_MOVE_MAX = " + HAND_MOVE_MAX);
            writer.newLine();
            writer.append("PIXEL_COUNT_TRACK_MIN = " + PIXEL_COUNT_TRACK_MIN);
            writer.newLine();
            writer.append("PIXEL_COUNT_THRESHOLD_MIN = " + PIXEL_COUNT_THRESHOLD_MIN);
            writer.newLine();
            writer.append("PIXEL_COUNT_THRESHOLD_MAX = " + PIXEL_COUNT_THRESHOLD_MAX);
            writer.newLine();
            writer.append("YES_DELAY = " + YES_DELAY);
            writer.newLine();
            writer.append("PIXEL_HEIGHT = " + PIXEL_HEIGHT);
            writer.newLine();
            writer.append("PIXEL_WIDTH = " + PIXEL_WIDTH);
            writer.newLine();
            writer.append("LOAD_INFO_KINECT_IN_USE_DELAY = " + LOAD_INFO_KINECT_IN_USE_DELAY);
            writer.newLine();
            writer.append("ONLY_ONE_BLOB = " + (ONLY_ONE_BLOB ? 1 : 0));
            writer.newLine();
            writer.append("HELP_MSG = " + (HELP_MSG ? 1 : 0));
            writer.newLine();
            writer.append("DEBUG = " + (DEBUG ? 1 : 0));
            writer.newLine();
            writer.append("AUTOMATIC_SLIDE_TIME = " + AUTOMATIC_SLIDE_TIME);
            writer.newLine();
            writer.append("AUTOMATIC_INIT = " + (AUTOMATIC_INIT ? 1 : 0));
            writer.newLine();
            writer.append("HAND_TRACKER = " + (HAND_TRACKER ? 1 : 0));
            writer.newLine();
            writer.append("THRESHOLD_MAX = " + THRESHOLD_MAX);
            writer.newLine();
            
            writer.close();
            
            LogReport.insertLog("Configurações salvas com sucesso.", null, true);
            
            loadConstants();
        } catch (Exception e) {
            LogReport.insertLog("Erro ao savar configurações.", e, true);
        }

    }
    
    public static void saveConstants(Config config) {
        try {
            String constants = "constants.txt";
            File file = new File(constants);
            if (!file.exists()) {
                file.createNewFile();
            }
            
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.append("PICTURE_REFRESH_TIME = " + config.PICTURE_REFRESH_TIME);
            writer.newLine();
            writer.append("HAND_THRESHOLD = " + config.HAND_THRESHOLD);
            writer.newLine();
            writer.append("HAND_MOVE_MIN = " + config.HAND_MOVE_MIN);
            writer.newLine();
            writer.append("HAND_MOVE_MAX = " + config.HAND_MOVE_MAX);
            writer.newLine();
            writer.append("PIXEL_COUNT_TRACK_MIN = " + config.PIXEL_COUNT_TRACK_MIN);
            writer.newLine();
            writer.append("PIXEL_COUNT_THRESHOLD_MIN = " + config.PIXEL_COUNT_THRESHOLD_MIN);
            writer.newLine();
            writer.append("PIXEL_COUNT_THRESHOLD_MAX = " + config.PIXEL_COUNT_THRESHOLD_MAX);
            writer.newLine();
            writer.append("YES_DELAY = " + config.YES_DELAY);
            writer.newLine();
            writer.append("PIXEL_HEIGHT = " + config.PIXEL_HEIGHT);
            writer.newLine();
            writer.append("PIXEL_WIDTH = " + config.PIXEL_WIDTH);
            writer.newLine();
            writer.append("LOAD_INFO_KINECT_IN_USE_DELAY = " + config.LOAD_INFO_KINECT_IN_USE_DELAY);
            writer.newLine();
            writer.append("ONLY_ONE_BLOB = " + (config.ONLY_ONE_BLOB ? 1 : 0));
            writer.newLine();
            writer.append("HELP_MSG = " + (config.HELP_MSG ? 1 : 0));
            writer.newLine();            
            writer.append("DEBUG = " + (config.DEBUG ? 1 : 0));
            writer.newLine();
            writer.append("AUTOMATIC_SLIDE_TIME = " + config.AUTOMATIC_SLIDE_TIME);
            writer.newLine();
            writer.append("AUTOMATIC_INIT = " + (config.AUTOMATIC_INIT ? 1 : 0));
            writer.newLine();            
            writer.append("HAND_TRACKER = " + (config.HAND_TRACKER ? 1 : 0));
            writer.newLine();
            writer.append("THRESHOLD_MAX = " + config.THRESHOLD_MAX);
            writer.newLine();
            
            writer.close();
            
            LogReport.insertLog("Configurações salvas com sucesso.", null, true);
            
            loadConstants();
        } catch (Exception e) {
            LogReport.insertLog("Erro ao savar configurações.", e, true);
        }

    }

    public static void loadConstants() {
        try {
            String constants = "constants.txt";
            File file = new File(constants);
            if (!file.exists()) {
                return;
            }
            props = new Properties();
            FileInputStream fis;
            try {
                fis = new FileInputStream(constants);
                props.load(fis);
                fis.close();

                PICTURE_REFRESH_TIME = Integer.valueOf(props.getProperty("PICTURE_REFRESH_TIME"));
                HAND_THRESHOLD = Integer.valueOf(props.getProperty("HAND_THRESHOLD"));
                HAND_MOVE_MIN = Integer.valueOf(props.getProperty("HAND_MOVE_MIN"));
                HAND_MOVE_MAX = Integer.valueOf(props.getProperty("HAND_MOVE_MAX"));
                PIXEL_COUNT_TRACK_MIN = Integer.valueOf(props.getProperty("PIXEL_COUNT_TRACK_MIN"));
                PIXEL_COUNT_THRESHOLD_MIN = Integer.valueOf(props.getProperty("PIXEL_COUNT_THRESHOLD_MIN"));
                PIXEL_COUNT_THRESHOLD_MAX = Integer.valueOf(props.getProperty("PIXEL_COUNT_THRESHOLD_MAX"));
                YES_DELAY = Integer.valueOf(props.getProperty("YES_DELAY"));
                PIXEL_HEIGHT = Integer.valueOf(props.getProperty("PIXEL_HEIGHT"));
                PIXEL_WIDTH = Integer.valueOf(props.getProperty("PIXEL_WIDTH"));
                LOAD_INFO_KINECT_IN_USE_DELAY = Integer.valueOf(props.getProperty("LOAD_INFO_KINECT_IN_USE_DELAY"));
                ONLY_ONE_BLOB = Integer.valueOf(props.getProperty("ONLY_ONE_BLOB")) == 1;
                HELP_MSG = Integer.valueOf(props.getProperty("HELP_MSG")) == 1;
                DEBUG = Integer.valueOf(props.getProperty("DEBUG")) == 1;
                AUTOMATIC_SLIDE_TIME = Integer.valueOf(props.getProperty("AUTOMATIC_SLIDE_TIME"));
                HAND_TRACKER = Integer.valueOf(props.getProperty("HAND_TRACKER")) == 1;
                AUTOMATIC_INIT = Integer.valueOf(props.getProperty("AUTOMATIC_INIT")) == 1;
                THRESHOLD_MAX = Integer.valueOf(props.getProperty("THRESHOLD_MAX"));

            } catch (FileNotFoundException e) {
                LogReport.insertLog("Arquivo de configuração não encontrado.", e, true);
            } catch (IOException e) {
                LogReport.insertLog("Erro de IO.", e, true);
            }
        } catch (Exception e) {
            LogReport.insertLog("Erro ao carregar as configurações padrões.", e, true);
        }
    }

}
