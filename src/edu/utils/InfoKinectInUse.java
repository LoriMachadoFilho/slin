/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utils;

/**
 *
 * @author zinho
 */
public class InfoKinectInUse {
    
    public int PICTURE_REFRESH_TIME;
    public int HAND_THRESHOLD_X;
    public int HAND_THRESHOLD_Y;
    public int HAND_MOVE;
    public int PIXEL_COUNT_TRACK;
    public int PIXEL_COUNT_THRESHOLD;
    public int YES_DELAY;
    public int PIXEL_HEIGHT;
    public int PIXEL_WIDTH;
    public int BLOBS_NUMBER;
    public int FRAME_RATE;
    public int THRESHOLD;
    public int TILT;
    
}
