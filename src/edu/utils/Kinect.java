/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utils;

import edu.gui.principal.JPrincipal;

/**
 *
 * @author Zinho
 */
public class Kinect {

    private static Thread checkKinect;

    public static void keepCheckingKinect(JPrincipal parent, boolean check) {
        if (!check && checkKinect != null) {
            checkKinect.interrupt();
        }
        if (check) {
            checkKinect = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        while (true) {
                            parent.setTextButtonOnOff(edu.gui.principal.JPrincipal.kinectProcessing != null ? "Sim" : "N�o");
                            Thread.sleep(2000);
                        }
                    } catch (InterruptedException ie) {
                        System.out.println("Thread Check Kinect Interrompida!");
                    } catch (Exception ex) {
                        System.out.println("Erro na Thread Check Kinect. Tentando iniciar novamente...");
                        keepCheckingKinect(parent, check);
                    }
                }
            });
            checkKinect.start();
        }

    }
}
