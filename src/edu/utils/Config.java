/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.utils;

/**
 *
 * @author zinho
 */
public class Config {

    public int PICTURE_REFRESH_TIME;
    public int HAND_THRESHOLD;
    public int HAND_MOVE_MIN;
    public int HAND_MOVE_MAX;
    public int PIXEL_COUNT_TRACK_MIN;
    public int PIXEL_COUNT_THRESHOLD_MIN;
    public int PIXEL_COUNT_THRESHOLD_MAX;
    public int YES_DELAY;
    public int PIXEL_HEIGHT;
    public int PIXEL_WIDTH;
    public int LOAD_INFO_KINECT_IN_USE_DELAY;
    public boolean ONLY_ONE_BLOB;
    public boolean HELP_MSG;
    public boolean DEBUG;
    public int AUTOMATIC_SLIDE_TIME;
    public boolean HAND_TRACKER;
    public boolean AUTOMATIC_INIT;
    public int THRESHOLD_MAX;
    public int THRESHOLD_MIN;
}
